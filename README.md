# Ingress

A load-balancer using [traefik](https://traefik.io/) for
reverse proxying.

## Usage

1. Please prepare the stack with the following setup steps.

    ```
    make setup
    ```

2. After that you can start the ingress

    ```bash
    docker compose up -d
    ```

3. You can take a look at the running stack with

    ```bash
    docker compose ps
    ```

    and at the logs with

    ```bash
    docker compose logs -f
    ```
